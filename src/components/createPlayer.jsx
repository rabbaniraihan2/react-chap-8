import React from "react";

export default function CreatePlayer(props) {
    const variable = props.variable

    return (
        <div >
            <form action="">
                <ul>
                    <li>
                        <p>{props.type}</p>
                        <input type="text" value={variable} onChange={props.handleChange} />
                    </li>
                    <li>
                        <button
                            onClick={props.handleSave}
                        >Save</button>
                    </li>
                </ul>
            </form>
        </div>
    );
}
