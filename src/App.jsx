
import './App.css';
import CreatePlayer from './components/createPlayer'
import { useState } from 'react';

function App() {
  const [username, setUsername] = useState('')
  const [email, setEmail] = useState('')
  const [experience, setExperience] = useState('')
  const [level, setLevel] = useState('')

  async function handleChange(e) {
    let val = e.target.value
    setUsername(val)
  }

  async function handleSave() {
    try {

    } catch (error) {
      console.log(error)
    }
  }

  return (
    <>
      <h1>HOME</h1>
      <h1>Create Player</h1>
      <CreatePlayer
        variable={username}
        handleChange={handleChange}
        handleSave={handleSave}
        type="Username"
      />
      <CreatePlayer
        variable={email}
        handleChange={handleChange}
        handleSave={handleSave}
        type="Email"
      />
      <CreatePlayer
        variable={experience}
        handleChange={handleChange}
        handleSave={handleSave}
        type="Experience"
      />
      <CreatePlayer
        variable={level}
        handleChange={handleChange}
        handleSave={handleSave}
        type="Level"
      />
    </>
  );
}

export default App;
